# class which defines cell #
from graphics import *
win = GraphWin("My Window", 500, 500)

class Cell:
    def __init__(self,x,y,state,mates):
        self.x = x
        self.y = y
        self.state = state
        self.mates = mates


    def draw_cell(self):
        square = Rectangle(Point(self.x-5,self.y-5), Point(self.x+5,self.y+5))
        if self.state == 'alive':
            square.setFill('white')
        if self.state == 'dead':
            square.setFill('black')
        square.draw(win)